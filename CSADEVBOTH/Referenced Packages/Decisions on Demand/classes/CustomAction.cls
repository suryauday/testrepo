/*
This file is generated and isn't the actual source code for this
managed global class.
This read-only file shows the class's global constructors,
methods, variables, and properties.
To enable code to compile, all methods return null.
*/
global virtual class CustomAction implements DecsOnD.IPolicyAction {
    global static String NAME;
    global Map<String,Object> parameters {
        get;
    }
    global CustomAction() {

    }
    global virtual void apply(DecsOnD.PolicyActionRecord actionRec) {

    }
    global virtual String getLabel(String locale) {
        return null;
    }
    global virtual String getName() {
        return null;
    }
    global Object getParameter(String paramName) {
        return null;
    }
    global virtual void prepare(DecsOnD.PolicyActionRecord actionRec) {

    }
}
